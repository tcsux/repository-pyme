
//Validaciones Persona Física
(function () {
      IncializarComponentes();

      function IncializarComponentes() {
        document.getElementById("continuar").disabled = true;
      }
    })();

    function validar() {
      document.getElementById("continuar").disabled = !ValidarForm();
		
      function ValidarForm()
      {
        var sectorIndustrial = document.getElementById("sectorIndustrial").value;
		var edad = document.getElementById("edad").value;
		var rangoA = document.getElementById("rangoA").checked;
        var rangoB = document.getElementById("rangoB").checked;
		var rangoC = document.getElementById("rangoC").checked;
		var rangoD = document.getElementById("rangoD").checked;
		var rangoE = document.getElementById("rangoE").checked; 
		var antiguedadA = document.getElementById("antiguedadA").checked;
        var antiguedadB = document.getElementById("antiguedadB").checked;
		var antiguedadC = document.getElementById("antiguedadC").checked;
		var antiguedadD = document.getElementById("antiguedadD").checked;
		var antiguedadE = document.getElementById("antiguedadE").checked;
		var clienteA = document.getElementById("clienteA").checked;
        var clienteB = document.getElementById("clienteB").checked;
		var creditosBancariosA = document.getElementById("creditosBancariosA").checked;
        var creditosBancariosB = document.getElementById("creditosBancariosB").checked;
		
        if (sectorIndustrial == 0) {
          return false;
        }		  
      
        if (!/^([0-9])*$/.test(edad))
          alert("El valor " + edad + " no es un número");
      		  
        if (edad == 0) {
          return false;
        }
	

        if (!rangoA && !rangoB && !rangoC && !rangoD && !rangoE) {
          return false;
        }
		  
		if (!antiguedadA && !antiguedadB && !antiguedadC && !antiguedadD && !antiguedadE) {
          return false;
        }
		  if (!creditosBancariosB ) {
          return false;
        }
        return true;
      }
    }

function PFcreditosBancariosA() {
  document.getElementById("continuar").disabled = !ValidarForm();
  function ValidarForm()
  {
	var creditosBancariosA = document.getElementById("creditosBancariosA").checked;

if (!creditosBancariosA ) {
	  return false;
	}
	return false;
  }
}
function PFatraso() {
      document.getElementById("continuar").disabled = !ValidarForm();
      function ValidarForm()
      {
		var atrasoA = document.getElementById("atrasoA").checked;
        var atrasoB = document.getElementById("atrasoB").checked;
	if (!atrasoB ) {
          return false;
        }
        return true;
      }
    }
function PFatrasoMonto() {
      document.getElementById("continuar").disabled = !ValidarForm();
      function ValidarForm()
      {
		var atrasoMontoA = document.getElementById("atrasoMontoA").checked;
        var atrasoMontoB = document.getElementById("atrasoMontoB").checked;
	if (!atrasoMontoA && !atrasoMontoB ) {
          return false;
        }
        return true;
      }
    }





//Validaciones Persona Moral
(function () {
      IncializarComponentes2();

      function IncializarComponentes2() {
        document.getElementById("continuar2").disabled = true;
      }
    })();

 function validar2() {
      document.getElementById("continuar2").disabled = !ValidarForm();

      function ValidarForm()
      {
        var sectorIndustrial = document.getElementById("sectorIndustrial").value;
		  var tipoSociedad = document.getElementById("tipoSociedad").value;
		          
		var rangoA = document.getElementById("rangoA").checked;
        var rangoB = document.getElementById("rangoB").checked;
		var rangoC = document.getElementById("rangoC").checked;
		var rangoD = document.getElementById("rangoD").checked;
		var rangoE = document.getElementById("rangoE").checked; 
		  
		  
		var antiguedadA = document.getElementById("antiguedadA").checked;
        var antiguedadB = document.getElementById("antiguedadB").checked;
		var antiguedadC = document.getElementById("antiguedadC").checked;
		var antiguedadD = document.getElementById("antiguedadD").checked;
		var antiguedadE = document.getElementById("antiguedadE").checked;
		  
		var clienteA = document.getElementById("clienteA").checked;
        var clienteB = document.getElementById("clienteB").checked;
		  
		var creditosBancariosA = document.getElementById("creditosBancariosA").checked;
        var creditosBancariosB = document.getElementById("creditosBancariosB").checked;
		  
		var atrasoA = document.getElementById("atrasoA").checked;
        var atrasoB = document.getElementById("atrasoB").checked;  
		

        if (sectorIndustrial == 0) {
          return false;
        }

        if (tipoSociedad == 0) {
          return false;
        }

        if (!rangoA && !rangoB && !rangoC && !rangoD && !rangoE) {
          return false;
        }
		  
		if (!antiguedadA && !antiguedadB && !antiguedadC && !antiguedadD && !antiguedadE) {
          return false;
        }
		  
		if (!clienteA && !clienteB ) {
          return false;
        }
		  
		if (!creditosBancariosB ) {
          return false;
        }
		
        return true;
		  
      }
    }

function PMcreditosBancariosA() {
  document.getElementById("continuar2").disabled = !ValidarForm();
  function ValidarForm()
  {
	var creditosBancariosA = document.getElementById("creditosBancariosA").checked;

if (!creditosBancariosA ) {
	  return false;
	}
	return false;
  }
}
function PMatraso() {
      document.getElementById("continuar2").disabled = !ValidarForm();
      function ValidarForm()
      {
		var atrasoA = document.getElementById("atrasoA").checked;
        var atrasoB = document.getElementById("atrasoB").checked;
	if (!atrasoB ) {
          return false;
        }
        return true;
      }
    }
function PMAtrasoMonto() {
      document.getElementById("continuar2").disabled = !ValidarForm();
      function ValidarForm()
      {
		var atrasoMontoA = document.getElementById("atrasoMontoA").checked;
        var atrasoMontoB = document.getElementById("atrasoMontoB").checked;
	if (!atrasoMontoA && !atrasoMontoB ) {
          return false;
        }
        return true;
      }
    }







  function justNumbers(e)
	{
		var keynum = window.event ? window.event.keyCode : e.which;
		if ((keynum == 8) || (keynum == 46))
		return true;

		return /\d/.test(String.fromCharCode(keynum));
	}


function validar_email( email ) 
{
    var regex = /^([a-zA-Z0-9_\.\-])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/;
    return regex.test(email) ? true : false;
}



