function validarFormulario(){
 
		var txtEdad = document.getElementById('edad').value;
		var cmbSelector = document.getElementById('selector').selectedIndex;
		var rbtEstado1 = document.getElementsByName('rangoDeVentasAnuales');
		var rbtEstado2 = document.getElementsByName('radioButton2');
		var rbtEstado3 = document.getElementsByName('radioButton3');
		var rbtEstado4 = document.getElementsByName('radioButton4');
 
		var banderaRBTN1 = false;
		var banderaRBTN2 = false;
		var banderaRBTN3 = false;
		var banderaRBTN4 = false;
 
		
 
		//Test edad
		if(txtEdad == null || txtEdad.length == 0 || isNaN(txtEdad)){
			alert('ERROR: Todos los campos son necesarios para poder continuar. Por favor ingresa tu edad.');
			return false;
		}
 
 
		//Test comboBox
		if(cmbSelector == null || cmbSelector == 0){
			alert('ERROR: Todos los campos son necesarios para poder continuar. Por favor elige un Sector Industrial.');
			return false;
		}
 
 
		//Test RadioButtons 1
		for(var i = 0; i < rbtEstado1.length; i++){
			if(rbtEstado1[i].checked){
				banderaRBTN1 = true;
				break;
			}
		}
		if(!banderaRBTN1){
			alert('ERROR: Todos los campos son necesarios para poder continuar. Por favor elige un Rango de ventas anuales.');
			return false;
		}
	
		
		//Test RadioButtons2
		for(var i = 0; i < rbtEstado2.length; i++){
			if(rbtEstado2[i].checked){
				banderaRBTN2 = true;
				break;
			}
		}
		if(!banderaRBTN2){
			alert('ERROR: Todos los campos son necesarios para poder continuar. Por favor elige un periodo de Antigüedad.');
			return false;
		}
	
		//Test RadioButtons3
		for(var i = 0; i < rbtEstado3.length; i++){
			if(rbtEstado3[i].checked){
				banderaRBTN3 = true;
				break;
			}
		}
		if(!banderaRBTN3){
			alert('ERROR: Todos los campos son necesarios para poder continuar. Por favor, indica si eres o no cliente de BBVA Bancomer.');
			return false;
		}
	
	//Test RadioButtons4
		for(var i = 0; i < rbtEstado4.length; i++){
			if(rbtEstado4[i].checked){
				banderaRBTN4 = true;
				break;
			}
		}
		if(!banderaRBTN4){
			alert('ERROR: Todos los campos son necesarios para poder continuar. Por favor, indica si tienes o no créditos bancarios.');
			return false;
		}
	
 
		return true;
	}

